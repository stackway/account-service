using System;
using System.Runtime.Serialization;

namespace AccountService.Application.Exceptions {
    [Serializable]
    public class AccessDeniedException<R> : Exception {
        public AccessDeniedException(R resource)
        : base($"illegal access to the resource: {resource}") { }

        public AccessDeniedException() : base("illegal access to the resource") { }

        public AccessDeniedException(string message) : base(message) { }
        public AccessDeniedException(string message, Exception inner)
        : base(message, inner) { }

        protected AccessDeniedException(
            SerializationInfo info,
            StreamingContext context
        ) : base(info, context) { }
    }
}