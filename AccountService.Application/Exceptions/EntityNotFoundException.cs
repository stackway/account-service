using System;
using System.Runtime.Serialization;

namespace AccountService.Application.Exceptions {
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="E">entity</typeparam>
    /// <typeparam name="K">entity Key</typeparam>
    [Serializable]
    public class EntityNotFoundException<E, K> : Exception {
        /// <summary>
        /// can be nullable
        /// </summary>
        public E NotFoundEntity { get; private set; }
        /// <summary>
        /// can be nullable
        /// </summary>
        public K NotFoundEntityKey { get; private set; }

        public EntityNotFoundException(E entity)
        : base($"entity '{entity}' not found") {
            NotFoundEntity = entity;
        }

        public EntityNotFoundException(K entityKey)
        : base($"entity not found by key '{entityKey}'") {
            NotFoundEntityKey = entityKey;
        }

        public EntityNotFoundException(E entity, K entityKey)
        : base($"entity '{entity}' not found by key '{entityKey}'") {
            NotFoundEntity = entity;
            NotFoundEntityKey = entityKey;
        }

        public EntityNotFoundException() : base("entity not found") { }


        public EntityNotFoundException(string message) : base(message) { }
        public EntityNotFoundException(string message, Exception inner)
        : base(message, inner) { }
        protected EntityNotFoundException(
            SerializationInfo info,
            StreamingContext context
        ) : base(info, context) { }
    }
}