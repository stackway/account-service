using System;
using System.Runtime.Serialization;

namespace AccountService.Application.Exceptions {
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="E">entity</typeparam>
    /// <typeparam name="K">entity key</typeparam>
    [Serializable]
    public class EntityExistException<E, K> : Exception {
        public E ExistEntity { get; private set; }
        public K ExistEntityKey { get; private set; }

        public EntityExistException(E entity)
        : base($"entity '{entity}' already exist") {
            ExistEntity = entity;
        }

        public EntityExistException(K entityKey)
        : base($"entity with key '{entityKey}' already exist") {
            ExistEntityKey = entityKey;
        }

        public EntityExistException(E entity, K entityKey)
        : base($"entity '{entity}' with key '{entityKey}' already exist") {
            ExistEntity = entity;
            ExistEntityKey = entityKey;
        }

        public EntityExistException() : base("entity already exist") { }

        public EntityExistException(string message) : base(message) { }
        public EntityExistException(string message, Exception inner)
        : base(message, inner) { }
        protected EntityExistException(
            SerializationInfo info,
            StreamingContext context
        ) : base(info, context) { }
    }
}