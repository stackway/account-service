using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;

using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

using AccountService.Application.Exceptions;
using AccountService.Application.Contracts;
using AccountService.Domain.Entities;
using AccountService.Infrastructure.Security;

namespace AccountService.Application.Services {
    public interface IUserTokenService {
        string GetToken(LoginModel login);
        string GetToken(User user);
    }
    public class UserTokenService : IUserTokenService {
        private readonly AccountContext _context;
        private readonly BearerTokensOptions _tokensOptions;
        private readonly ILogger _logger;

        public UserTokenService(
            AccountContext context,
            BearerTokensOptions tokensOptions,
            ILogger<UserTokenService> logger
        ) {
            _context = context;
            _tokensOptions = tokensOptions;
            _logger = logger;
        }

        /// <summary>
        /// get token for user with validation
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public string GetToken(LoginModel login) {
            User foundUser;

            try {
                foundUser = _context.Users.Single(u => u.Email == login.Email);
            }
            catch (ArgumentNullException) {
                var exception = new EntityNotFoundException<User, string>(login.Email);
                _logger.LogInformation(exception.Message);

                throw exception;
            }
            catch (InvalidOperationException) {
                var exception = new EntityNotFoundException<User, string>(login.Email);
                _logger.LogInformation(exception.Message);

                throw exception;
            }

            var hashedPassword = new Pbkdf2HashedPassword(
                login.Password, foundUser.HashSalt
            );

            if (foundUser.PasswordHash != hashedPassword.Hash) {
                throw new ArgumentException("invalid login or password");
            }

            return GetToken(foundUser);
        }

        /// <summary>
        /// get token for user without validation
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public string GetToken(User user) {
            var claims = new List<Claim> {
                    new Claim(ClaimTypes.Name, user.Email),
                    new Claim(UserClaimsTypes.Id, $"{user.Id}")
                };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateJwtSecurityToken(
                issuer: _tokensOptions.Issuer,
                audience: _tokensOptions.Audience,
                subject: new ClaimsIdentity(claims),
                notBefore: DateTime.Now,
                expires: DateTime.Now.Add(
                    TimeSpan.FromHours(_tokensOptions.AccessTokenExpirationHours)
                ),
                issuedAt: DateTime.Now,
                signingCredentials: new SigningCredentials(
                    _tokensOptions.GetSymmetricSignSecurityKey(),
                    SecurityAlgorithms.HmacSha256
                ),
                encryptingCredentials: new EncryptingCredentials(
                    _tokensOptions.GetSymmetricEncryptionKey(),
                    JwtConstants.DirectKeyUseAlg,
                    SecurityAlgorithms.Aes256CbcHmacSha512
                )
            );

            return tokenHandler.WriteToken(token);
        }
    }
}