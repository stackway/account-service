using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using AccountService.Domain.Entities;
using AccountService.Application.Exceptions;
using AccountService.Application.Contracts;
using AccountService.Domain.Units;

namespace AccountService.Application.Services {
    public interface IUserService {
        User FindById(long id);
        User FindByEmail(string email);
        int AddUser(User user);
        User RemoveUser(long id);
    }

    public class UserService : IUserService {
        private readonly AccountContext _context;
        private readonly IPrepareUserWorkspaceUnit _userWorkspace;

        public UserService(
            AccountContext context,
            IPrepareUserWorkspaceUnit userWorkspace
        ) {
            _context = context;
            _userWorkspace = userWorkspace;
        }

        public User FindById(long id) {
            try {
                return _context.Users
                    .AsNoTracking()
                    .Single(u => u.Id == id);
            }
            catch (InvalidOperationException) {
                throw new EntityNotFoundException<User, long>(id);
            }
        }

        public User FindByEmail(string email) {
            try {
                return _context.Users
                    .AsNoTracking()
                    .Single(u => u.Email == email);
            }
            catch (InvalidOperationException) {
                throw new EntityNotFoundException<User, string>(email);
            }
        }

        public int AddUser(User user) {
            if (_context.Users.Any(u => u.Email == user.Email))
                throw new EntityExistException<User, long>(user);

            _context.Users.Add(
                _userWorkspace.PrepareUserWorkspace(user)
            );
            return _context.SaveChanges();
        }

        public User RemoveUser(long id) {
            var user = _context.Users.Find(id);

            if (user == null) {
                throw new EntityNotFoundException<User, long>(id);
            }

            _context.Users.Remove(user);
            _context.SaveChanges();

            return user;
        }
    }
}