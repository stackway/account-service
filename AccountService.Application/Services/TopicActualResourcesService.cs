using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AccountService.Domain.Entities;
using AccountService.Application.Exceptions;
using AccountService.Application.Contracts;
using AutoMapper;
using LanguageExt.UnsafeValueAccess;
using Z.EntityFramework.Plus;

namespace AccountService.Application.Services {
    public interface ITopicActualResourcesService {
        IEnumerable<ActualResource> FindActualResources(long topicId);
        ActualResource FindActualResource(long topicId, long resourceId);
        Task<int> AddActualResourcesAsync(long topicId, IEnumerable<ActualResource> resources);
        int AddActualResources(long topicId, IEnumerable<ActualResource> resources);
        int ArchiveActualResource(long topicId, long resourceId);
        void RemoveActualResource(long topicId, long resourceId);
        void RemoveActualResources(long topicId);
    }

    public class TopicActualResourcesService : ITopicActualResourcesService {
        private readonly AccountContext _context;
        private readonly IUserArchivedResourcesService _archivingService;
        private readonly ITagsService _tagsService;
        private readonly IMapper _mapper;

        public TopicActualResourcesService(
            AccountContext context,
            IUserArchivedResourcesService archivingService,
            ITagsService tagsService, IMapper mapper) {
            _context = context;
            _archivingService = archivingService;
            _tagsService = tagsService;
            _mapper = mapper;
        }

        public IEnumerable<ActualResource> FindActualResources(long topicId) {
            return _context.ActualResources
                .Where(a => a.Topic.Id == topicId);
        }

        public ActualResource FindActualResource(long topicId, long resourceId) {
            var actualResource = _context.ActualResources
                .SingleOrDefault(a => a.Topic.Id == topicId && a.Id == resourceId);

            if (actualResource == null) {
                throw new EntityNotFoundException<ActualResource, long>(resourceId);
            }

            return actualResource;
        }

        public async Task<int> AddActualResourcesAsync(
            long topicId, IEnumerable<ActualResource> resources
        ) {
            var topic = await _context.Topics.FindAsync(topicId);

            if (topic == null) {
                return 0;
            }

            topic.ActualResources = resources.ToList();

            return await _context.SaveChangesAsync();
        }

        public int AddActualResources(
            long topicId, IEnumerable<ActualResource> resources
        ) {
            var topic = _context.Topics.Find(topicId);

            if (topic == null) {
                return 0;
            }

            topic.ActualResources = resources.ToList();

            return _context.SaveChanges();
        }

        public int ArchiveActualResource(long topicId, long resourceId) {
            var actualResource = _context
                .ActualResources
                .SingleOrDefault(a => a.Id == resourceId && a.Topic.Id == topicId);

            if (actualResource == null) {
                throw new EntityNotFoundException<ActualResource, long>(resourceId);
            }

            _archivingService.AddByTopic(
                topicId,
                _mapper
                    .Map<ActualResource, ArchivedResource>(actualResource)
                    .Setup.ArchiveNow().Done
            );

            _tagsService.MarkAsUsed(actualResource);

            return _context.ActualResources
                .Remove(actualResource)
                .Context.SaveChanges();
        }

        public void RemoveActualResource(long topicId, long resourceId) {
            throw new System.NotImplementedException();
        }

        public void RemoveActualResources(long topicId) {
            _context.ActualResources
                .Where(a => a.Topic.Id == topicId)
                .Delete();

            _context.SaveChanges();
        }
    }
}