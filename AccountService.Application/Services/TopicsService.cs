using AccountService.Domain.Entities;
using AccountService.Application.Exceptions;

namespace AccountService.Application.Services {

    public interface ITopicsService {
        void UpdateTopic(long id, Topic topic);
    }

    public class TopicsService : ITopicsService {

        private AccountContext _context;

        public TopicsService(AccountContext context) {
            _context = context;
        }

        public void UpdateTopic(long id, Topic topic) {
            var foundTopic = _context.Topics.Find(id);

            if (foundTopic == null)
                throw new EntityNotFoundException<Topic, long>(topic, id);

            foundTopic.Setup()
                .SetName(topic.Name);

            _context.SaveChanges();
        }
    }
}