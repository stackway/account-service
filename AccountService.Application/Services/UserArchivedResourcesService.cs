using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using AccountService.Domain.Entities;
using AccountService.Application.Exceptions;
using AutoMapper;

namespace AccountService.Application.Services {
    public interface IUserArchivedResourcesService {
        int AddByUser(long userId, ArchivedResource resource);
        int AddByUser(User user, ArchivedResource resource);
        int AddByTopic(long topicId, ArchivedResource resource);
        int AddByActualResource(long userId, long actualResourceId);
        IEnumerable<string> GetUnique(long userId, IEnumerable<string> links);
    }

    public class UserArchivedResourcesService : IUserArchivedResourcesService {
        private readonly AccountContext _context;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;

        public UserArchivedResourcesService(
            AccountContext context,
            IMapper mapper,
            ILogger<UserArchivedResourcesService> logger
        ) {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public int AddByUser(long userId, ArchivedResource resource) {
            var user = _context.Users.Find(userId);

            if (user == null) {
                throw new EntityNotFoundException<User, long>(userId);
            }

            user.ArchivedResources.Add(resource);

            return _context.SaveChanges();
        }

        public int AddByUser(User user, ArchivedResource resource) {
            user.ArchivedResources.Add(resource);

            return _context.SaveChanges();
        }

        public int AddByTopic(long topicId, ArchivedResource resource) {
            var topic = _context.Topics
                .Include(t => t.User)
                .SingleOrDefault(t => t.Id == topicId);

            if (topic == null) {
                throw new EntityNotFoundException<Topic, long>(topicId);
            }

            topic.User.ArchivedResources.Add(resource);

            return _context.SaveChanges();
        }

        public int AddByActualResource(long userId, long actualResourceId) {
            var user = _context.Users
                .Include(u => u.ArchivedResources)
                .SingleOrDefault(u => u.Id == userId);

            if (user == null) {
                throw new EntityNotFoundException<User, long>(userId);
            }

            var actualResource = _context.ActualResources.Find(actualResourceId);

            if (actualResource == null) {
                throw new EntityNotFoundException<ActualResource, long>(actualResourceId);
            }
            
            user.ArchivedResources.Add(_mapper
                .Map<ActualResource, ArchivedResource>(actualResource)
                .Setup.ArchiveNow().Done
            );

            _context.ActualResources.Remove(actualResource);

            return _context.SaveChanges();
        }

        public IEnumerable<string> GetUnique(long userId, IEnumerable<string> links) {
            try {
                var wasReed = _context.ArchivedResources
                    .Where(a => a.User.Id == userId && links.Contains(a.Link))
                    .Select(a => a.Link);

                return links.Filter(link => !wasReed.Contains(link));
            }
            catch (ArgumentNullException exception) {
                _logger.LogInformation(
                    "can't find user history: {message}",
                    exception.Message
                );

                return new List<string>();
            }
        }
    }
}