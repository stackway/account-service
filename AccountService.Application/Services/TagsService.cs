using System;
using System.Linq;

using AccountService.Domain.Entities;
using AccountService.Application.Exceptions;

namespace AccountService.Application.Services {
    public interface ITagsService {
        int MarkAsUsed(ActualResource resource);
        void UpdateTag(long id, Tag tag);
    }

    public class TagsService : ITagsService {
        private AccountContext _context;

        public TagsService(AccountContext context) {
            _context = context;
        }

        public int MarkAsUsed(ActualResource resource) {
            var tagsToUpdate = _context.Tags
                .Where(t => resource.QueryTags.Contains(t.Name));

            foreach (var tag in tagsToUpdate) {
                tag.Setup().ViewNow();
            }

            return _context.SaveChanges();
        }

        public void UpdateTag(long id, Tag tag) {
            var foundTag = _context.Tags.Find(id);

            if (foundTag == null) {


                throw new EntityNotFoundException<Tag, long>(tag, id);
            }

            if (id != tag.Id || tag.Id != foundTag.Id) {
                throw new AccessDeniedException<Tag>(tag);
            }

            _context.Update(tag);

            _context.SaveChanges();
        }
    }
}