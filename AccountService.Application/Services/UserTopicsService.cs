using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AccountService.Domain.Entities;
using AccountService.Application.Exceptions;
using AccountService.Application.Contracts;
using Microsoft.Extensions.Logging;

namespace AccountService.Application.Services {
    public interface IUserTopicsService {
        bool HasResources(long userId);
        IEnumerable<Topic> FindTopics(long userId);
        Topic FindTopic(long userId, long topicId);
        int AddTopic(long userId, Topic topic);
        int RemoveTopic(long userId, long topicId);
    }

    public class UserTopicsService : IUserTopicsService {
        private readonly AccountContext _context;
        private readonly ILogger _logger;

        public UserTopicsService(
            AccountContext context,
            ILogger<UserTopicsService> logger
        ) {
            _context = context;
            _logger = logger;
        }

        public bool HasResources(long userId) {
            return _context.ActualResources
                .AsNoTracking()
                .Any(resource => resource.Topic.User.Id == userId);
        }

        public IEnumerable<Topic> FindTopics(long userId) {
            return _context.Topics
                .Include(t => t.ActualResources)
                .AsNoTracking()
                .Where(t => t.User.Id == userId);
        }

        public Topic FindTopic(long userId, long topicId) {
            try {
                return _context.Topics
                    .AsNoTracking()
                    .Include(t => t.ActualResources)
                    .Single(t => t.User.Id == userId && t.Id == topicId);
            }
            catch (InvalidOperationException) {
                throw new EntityNotFoundException<Topic, long>(topicId);
            }
        }

        public int AddTopic(long userId, Topic topic) {
            var hasTopic = _context
                .Topics
                .Any(t => t.User.Id == userId && t.Id == topic.Id);

            if (hasTopic)
                throw new EntityExistException<Topic, long>(topic);

            var user = _context.Users.Find(userId);

            if (user == null)
                throw new EntityNotFoundException<User, long>(userId);

            user.SetupTopics().AddTopic(topic);

            return _context.SaveChanges();
        }

        public int RemoveTopic(long userId, long topicId) {
            try {
                _context.Topics.Remove(
                    _context.Topics.Single(t => t.User.Id == userId && t.Id == topicId)
                );

                return _context.SaveChanges();
            }
            catch (InvalidOperationException) {
                throw new EntityNotFoundException<Topic, long>(topicId);
            }
        }
    }
}