using System.Collections.Generic;
using System.Linq;
using AccountService.Application.Exceptions;
using AccountService.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Z.EntityFramework.Plus;

namespace AccountService.Application.Services {
    public interface IUserCommunitiesService {
        IEnumerable<User> GetSimilarUsers(long userId);
        IEnumerable<ArchivedResource> GetResources(long userId, int takeFromUser);
        int AddSimilarUsers(long id, IEnumerable<long> similar);
        void RemoveSimilarUsers(long userId);
    }

    public class UserCommunitiesService : IUserCommunitiesService {
        private readonly AccountContext _context;
        private readonly ILogger _logger;

        public UserCommunitiesService(
            AccountContext context,
            ILogger<UserCommunitiesService> logger
        ) {
            _context = context;
            _logger = logger;
        }

        public IEnumerable<User> GetSimilarUsers(long userId) {
            var user = _context.Users
                .Include(u => u.OriginalCommunities)
                .SingleOrDefault(u => u.Id == userId);

            if (user == null) {
                throw new EntityNotFoundException<User, long>(userId);
            }

            return user.OriginalCommunities.Select(c => c.RelatedUser);
        }

        public IEnumerable<ArchivedResource> GetResources(long userId, int takeFromUser) {
            var users = GetSimilarUsers(userId);

            if (!users.Any()) {
                _logger.LogInformation(
                    "can't find any similar users for: {userId}",
                    userId
                );
            }

            var resources = new List<ArchivedResource>();

            foreach (var user in users) {
                resources.AddRange(_context.ArchivedResources
                    .Where(a => a.User.Id == userId)
                    .OrderBy(a => a.CreatedAt)
                    .Take(takeFromUser)
                );
            }

            return resources;
        }

        public int AddSimilarUsers(long userId, IEnumerable<long> similar) {
            var user = _context.Users.Find(userId);

            if (user == null) {
                throw new EntityNotFoundException<User, long>(userId);
            }

            var foundSimilarUsers = _context.Users
                .Where(u => similar.Contains(u.Id));

            user
                .SetupCommunities()
                .UseCommunities(foundSimilarUsers);

            return _context.SaveChanges();
        }

        public void RemoveSimilarUsers(long userId) {
            _context.Communities
                .Where(c => c.OriginalUser.Id == userId)
                .Delete();

            _context.SaveChanges();

            _logger.LogInformation(
                "similar users was removed for: {userId}",
                userId
            );
        }
    }
}