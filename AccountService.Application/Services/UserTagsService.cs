using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using AccountService.Application.Exceptions;
using AccountService.Domain.Entities;

namespace AccountService.Application.Services {
    public interface IUserTagsService {
        bool HasTags(long userId);
        IEnumerable<Tag> FindTags(long userId);
        IEnumerable<Tag> FindAviableTags(long userId);
        Tag FindTag(long userId, long tagId);
        int AddTag(long userId, Tag tag);
        int RemoveTag(long userId, long tagId);
    }

    public class UserTagsService : IUserTagsService {
        private readonly AccountContext _context;

        public UserTagsService(AccountContext context) {
            _context = context;
        }

        public bool HasTags(long userId) {
            return _context.Tags
                .AsNoTracking()
                .Any(tag => tag.User.Id == userId);
        }

        public IEnumerable<Tag> FindTags(long userId) {
            return _context.Tags
                .AsNoTracking()
                .Where(t => t.User.Id == userId);
        }

        public IEnumerable<Tag> FindAviableTags(long userId) {
            return _context.Tags
                .AsNoTracking()
                .Where(t => t.User.Id == userId && t.IsHidden == false);
        }

        public Tag FindTag(long userId, long tagId) {
            try {
                return _context.Tags
                    .AsNoTracking()
                    .Single(t => t.User.Id == userId);
            }
            catch (InvalidOperationException) {
                throw new EntityNotFoundException<Tag, long>(tagId);
            }
        }

        public int AddTag(long userId, Tag tag) {
            var hasTag = _context
                .Tags
                .Any(t => t.User.Id == userId && t.Id == tag.Id);

            if (hasTag) {
                throw new EntityExistException<Tag, long>(tag);
            }

            var user = _context.Users.Find(userId);

            if (user == null) {
                throw new EntityNotFoundException<User, long>(userId);
            }

            user.Tags.Add(tag);

            return _context.SaveChanges();
        }

        public int RemoveTag(long userId, long tagId) {
            try {
                _context.Tags.Remove(
                    _context.Tags.Single(t => t.User.Id == userId && t.Id == tagId)
                );

                return _context.SaveChanges();
            }
            catch (InvalidOperationException) {
                throw new EntityNotFoundException<Tag, long>(tagId);
            }
        }
    }
}