using System;
using System.ComponentModel.DataAnnotations;

namespace AccountService.Application.Contracts {
    public class TagModel {
        public long Id { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(100)]
        public string Name { get; set; }
        public int Views { get; set; }
        public DateTime RecentlyUsed { get; set; }
        public bool IsHidden { get; set; }
        public bool IsDeleted { get; set; }
    }
}