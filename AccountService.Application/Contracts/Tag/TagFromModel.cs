using AccountService.Domain.Entities;

namespace AccountService.Application.Contracts {
    public class TagFromModel : Tag {
        public TagFromModel(TagModel tag) {
            Setup()
                .SetName(tag.Name)
                .SetViews(tag.Views)
                .SetVisibility(tag.IsHidden)
                .UpdateLastUsedDate(tag.RecentlyUsed);
        }
    }
}