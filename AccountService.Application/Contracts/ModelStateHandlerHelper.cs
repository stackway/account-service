using System.Collections;
using System.Linq;

using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace AccountService.Application.Contracts {
    public static class ModelStateHandlerHelper {
        public static IEnumerable Errors(this ModelStateDictionary modelState) {
            return modelState
                .Values
                .SelectMany(
                    entry => entry.Errors.Select(
                        error => error.ErrorMessage
                    )
                );
        }
    }

}