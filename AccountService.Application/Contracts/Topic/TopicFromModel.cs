using AccountService.Domain.Entities;

namespace AccountService.Application.Contracts {
    public class TopicFromModel : Topic {
        public TopicFromModel(TopicModel topic) {
            Setup()
                .SetName(topic.Name)
                .SetDescription(topic.Description);
        }
    }
}