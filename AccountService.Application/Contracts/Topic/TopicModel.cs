using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AccountService.Domain;
using AccountService.Domain.Entities;

namespace AccountService.Application.Contracts {
    public class TopicModel {
        public long Id { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(100)]
        public string Description { get; set; }

        public TopicModel() { }

        public TopicModel(Topic topic) {
            Id = topic.Id;
            Name = topic.Name;
            Description = topic.Description;
        }
    }
}