using System.ComponentModel.DataAnnotations;

using AccountService.Domain.Entities;
using AccountService.Infrastructure.Security;

namespace AccountService.Application.Contracts {
    public class LoginModel {
        [Required, EmailAddress]
        public string Email { get; set; }

        [Required, MinLength(8), MaxLength(18)]
        public string Password { get; set; }


        public override string ToString() => $"email: ${Email}, password: ${Password}";
    }

    public class UserFromLogin : User {
        public UserFromLogin(LoginModel model) {
            SetupLogin()
            .Email(model.Email)
            .Password(
                new Pbkdf2HashedPassword(model.Password)
            );
        }
    }
}