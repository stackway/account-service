using AccountService.Domain.Entities;

namespace AccountService.Application.Contracts {
    public class UserInfo {
        public long Id { get; private set; }
        public string Email { get; private set; }
        public string Username { get; private set; }
        public UserInfo(User user) {
            Id = user.Id;
            Email = user.Email;
            Username = user.Username;
        }
    }
}