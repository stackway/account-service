using System.ComponentModel.DataAnnotations;

using AccountService.Domain.Entities;
using AccountService.Infrastructure.Security;


namespace AccountService.Application.Contracts {
    public class RegisterModel {
        [Required, EmailAddress]
        public string Email { get; set; }

        [Required, MinLength(8), MaxLength(18)]
        public string Password { get; set; }

        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }

    public class UserFromRegister : User {
        public UserFromRegister(RegisterModel model) {
            SetupLogin()
            .Email(model.Email)
            .Password(
                new Pbkdf2HashedPassword(model.Password)
            );
        }
    }
}