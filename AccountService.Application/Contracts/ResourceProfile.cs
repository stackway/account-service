using AccountService.Domain.Entities;
using AutoMapper;

namespace AccountService.Application.Contracts {
    public class ResourceProfile : Profile {
        public ResourceProfile() {
            CreateMap<ActualResource, ArchivedResource>();
            CreateMap<ArchivedResource, ActualResource>();
        }
    }
}