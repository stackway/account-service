using System.Collections.Generic;
using AccountService.Domain.Entities;
using AccountService.Domain.Units;

namespace AccountService.Application.Units {
    public class BaseWorkspaceStarter : IPrepareUserWorkspaceUnit {
        private readonly AccountContext _context;

        public BaseWorkspaceStarter(AccountContext context) {
            _context = context;
        }

        public User PrepareUserWorkspace(User user) {
            user
                .SetupTopics()
                .UseTopics(new List<Topic>() {
                    new Topic()
                        .Setup()
                        .SetName("Line of your Trend")
                        .SetDescription("")
                        .SetType(TopicType.BY_USED_TAGS)
                        .Done(),

                    new Topic()
                        .Setup()
                        .SetName("Sometimes")
                        .SetDescription("")
                        .SetType(TopicType.BY_RECENTLY_VIEWED_TAGS)
                        .Done(),

                    new Topic()
                        .Setup()
                        .SetName("By the way")
                        .SetDescription("Something from your friends")
                        .SetType(TopicType.BY_SIMILAR_USERS)
                        .Done(),

                    new Topic()
                        .Setup()
                        .SetName("Why not?")
                        .SetDescription("You can missed it")
                        .SetType(TopicType.BY_RANDOM_TAGS)
                        .Done()
                });

            return user;
        }
    }
}