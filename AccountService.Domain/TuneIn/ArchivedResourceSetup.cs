using System;
using AccountService.Domain.Entities;

namespace AccountService.Domain.TuneIn {
    public class ArchivedResourceSetup {
        private readonly ArchivedResource _resource;
        
        public ArchivedResourceSetup(ArchivedResource resource) {
            _resource = resource;
        }

        public ArchivedResourceSetup ArchiveNow() {
            _resource.CreatedAt = DateTime.Now;

            return this;
        }

        public ArchivedResource Done => _resource;
    }
}