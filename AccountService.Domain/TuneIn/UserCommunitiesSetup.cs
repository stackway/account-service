using System;
using System.Collections.Generic;
using System.Linq;
using AccountService.Domain.Entities;

namespace AccountService.Domain.TuneIn {
    public class UserCommunitiesSetup {
        private readonly User _user;

        public UserCommunitiesSetup(User user) {
            _user = user;
        }

        public UserCommunitiesSetup UseCommunities(IEnumerable<User> similar) {
            _user.OriginalCommunities = similar
                .Select(u => new CommunityUser {
                    OriginalUser = _user,
                    OriginalUserId = _user.Id,
                    RelatedUser = u,
                    RelatedUserId = u.Id,
                    CreatedAt = DateTime.Now
                })
                .ToList();

            return this;
        }

        public User Done() {
            return _user;
        }
    }
}