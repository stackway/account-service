using System.Collections.Generic;

using AccountService.Domain.Entities;

namespace AccountService.Domain.TuneIn {
    public class TopicSetup {
        private Topic _topic;

        public TopicSetup(Topic topic) {
            _topic = topic;
        }

        public TopicSetup SetName(string name) {
            _topic.Name = name;

            return this;
        }

        public TopicSetup SetDescription(string description) {
            _topic.Description = description;

            return this;
        }

        public TopicSetup SetType(TopicType type) {
            _topic.Type = type;

            return this;
        }

        public Topic Done() {
            return _topic;
        }
    }
}