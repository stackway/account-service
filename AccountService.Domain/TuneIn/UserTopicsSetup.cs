using System.Collections.Generic;
using AccountService.Domain.Entities;

namespace AccountService.Domain.TuneIn {
    public class UserTopicsSetup {
        private User _user;

        public UserTopicsSetup(User user) {
            _user = user;
        }

        public UserTopicsSetup AddTopic(Topic topic) {
            _user.Topics.Add(topic);

            return this;
        }

        public UserTopicsSetup RemoveTopic(Topic topic) {
            _user.Topics.Remove(topic);

            return this;
        }

        public UserTopicsSetup UseTopics(ICollection<Topic> topics) {
            _user.Topics = topics;

            return this;
        }

        public User Done() {
            return _user;
        }
    }
}