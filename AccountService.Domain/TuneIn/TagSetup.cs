using System;

using AccountService.Domain.Entities;

namespace AccountService.Domain.TuneIn {
    public class TagSetup {
        private Tag _tag;

        public TagSetup(Tag tag) {
            _tag = tag;
        }

        public TagSetup SetName(string name) {
            _tag.Name = name;

            return this;
        }

        public TagSetup SetViews(int views) {
            _tag.Views = views;

            return this;
        }

        public TagSetup SetVisibility(bool isVisible) {
            _tag.IsHidden = isVisible;

            return this;
        }

        public TagSetup UpdateLastUsedDate(DateTime date) {
            _tag.RecentlyUsed = date;

            return this;
        }

        public TagSetup UseNow() {
            return UpdateLastUsedDate(DateTime.Now);
        }

        public TagSetup ViewNow() {
            _tag.Views += 1;

            return UseNow();
        }

        public TagSetup ResetViews() {
            _tag.Views = 0;

            return this;
        }

        public Tag Done() {
            return _tag;
        }
    }
}