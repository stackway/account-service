using System;
using System.Collections.Generic;
using System.Linq;

using AccountService.Domain.Entities;

namespace AccountService.Domain.TuneIn {
    public class ActualResourceSetup {
        private readonly ActualResource _actualResource;

        public ActualResourceSetup(ActualResource actualResource) {
            _actualResource = actualResource;
        }

        public ActualResourceSetup UseQueryTags(IEnumerable<string> tags) {
            _actualResource.QueryTags = tags.ToList();

            return this;
        }

        public ActualResource Done() {
            return _actualResource;
        }
    }
}