using System;
using System.Collections.Generic;

using AccountService.Domain.Entities;

namespace AccountService.Domain.TuneIn {
    public class ResourceSetup {
        private readonly Resource _resource;

        public ResourceSetup(Resource resource) {
            _resource = resource;
        }

        public ResourceSetup UseTags(Func<IEnumerable<string>, IEnumerable<string>> action) {
            _resource.Tags = action(_resource.Tags);

            return this;
        }

        public ResourceSetup UseTags(Action<IEnumerable<string>> action) {
            action(_resource.Tags);

            return this;
        }

        public Resource Done() {
            return _resource;
        }
    }
}