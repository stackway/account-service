using AccountService.Domain.Entities;

namespace AccountService.Domain.TuneIn {
    public class UserLoginSetup {
        private User _user { get; set; }

        public UserLoginSetup(User user) {
            _user = user;
        }

        public UserLoginSetup Email(string email) {
            _user.Email = email;

            return this;
        }

        public UserLoginSetup Password(string passwordHash, string hashSalt) {
            _user.PasswordHash = passwordHash;
            _user.HashSalt = hashSalt;

            return this;
        }

        public UserLoginSetup Password(HashedPassword hashedPassword) {
            return Password(hashedPassword.Hash, hashedPassword.Salt);
        }

        public User Done() {
            return _user;
        }
    }
}