using System.Collections.Generic;
using AccountService.Domain.Entities;

namespace AccountService.Domain.Units {
    public interface IUserWorkspaceUnit {
        void AddTopic(Topic topic, User user);
        void RemoveTopic(Topic topic, User user);

        void AddSourcesToBlackList(User user, params IgnoredSource[] sources);
        void AddSourcesToBlackList(User user, IEnumerable<IgnoredSource> sources);
        void RemoveSourcesFromBlackList(User user, params IgnoredSource[] sources);
        void RemoveSourcesFromBlackList(User user, IEnumerable<IgnoredSource> sources);
    }
}