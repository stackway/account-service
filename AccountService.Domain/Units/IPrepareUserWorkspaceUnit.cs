using AccountService.Domain.Entities;

namespace AccountService.Domain.Units {
    public interface IPrepareUserWorkspaceUnit {
        User PrepareUserWorkspace(User user);
    }
}