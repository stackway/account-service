using AccountService.Domain.Entities;

namespace AccountService.Domain.Units {
    public interface ITagsGraphWorkspaceUnit {
        void AddTagToGraph(Tag tag, User user);
        void AddTagWithoutRelations(Tag tag, User user);

        void AddTagsFromResource(Resource resource, User user);
        void AddTagFromResource(Resource resource, string tagName, User user);

        void HideTagFromGraph(Tag tag, User user);
        void ShowTagInGraph(Tag tag, User user);

        void DeleteTagFromGraph(Tag tag, User user);
    }
}