namespace AccountService.Domain.Entities {
    public class IgnoredSource {
        public long Id { get; set; }
        public string Value { get; set; }

        public BlackList BlackList { get; set; }

        public override bool Equals(object other) {
            if (other == null || GetType() != other.GetType())
                return false;

            var another = (other as IgnoredSource);

            return BlackList == another.BlackList && Value == another.Value;
        }

        public override int GetHashCode() {
            return BlackList.GetHashCode() + Value.GetHashCode();
        }
    }
}