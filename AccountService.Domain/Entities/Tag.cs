using System;
using System.Collections.Generic;
using AccountService.Domain.TuneIn;

namespace AccountService.Domain.Entities {
    public class Tag {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Views { get; set; }
        public DateTime RecentlyUsed { get; set; }
        public int Importance { get; set; }
        public bool IsHidden { get; set; } = false;

        public User User { get; set; }

        public TagSetup Setup() {
            return new TagSetup(this);
        }

        public override bool Equals(object other) {
            if (other == null || GetType() != other.GetType())
                return false;

            var tag = other as Tag;

            return Name == tag.Name && User == tag.User;
        }

        public override int GetHashCode() {
            var hashCode = Name.GetHashCode();

            if (User != null) {
                hashCode += User.GetHashCode();
            }

            return hashCode;
        }
    }
}