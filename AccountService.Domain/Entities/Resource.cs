using System;
using System.Collections.Generic;

namespace AccountService.Domain.Entities {
    public abstract class Resource {
        public abstract string DisplayLink { get; set; }
        public abstract string Link { get; set; }
        public abstract string Title { get; set; }
        public abstract string Description { get; set; }
        public abstract DateTime? PublishedAt { get; set; }
        public abstract string ImageLink { get; set; }
        public abstract string Type { get; set; }
        public abstract double RelevanceIndex { get; set; }

        public abstract IEnumerable<string> Tags { get; set; }
    }
}