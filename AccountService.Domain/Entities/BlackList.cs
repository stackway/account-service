using System.Collections.Generic;

namespace AccountService.Domain.Entities {
    public class BlackList {
        public long Id { get; set; }
        public HashSet<IgnoredSource> Sources { get; set; } = new HashSet<IgnoredSource>();

        public long UserForeignKey { get; set; }
        public User User { get; set; }

        public override bool Equals(object another) {
            if (another == null || GetType() != another.GetType())
                return false;

            return (another as BlackList).User == User;
        }

        public override int GetHashCode() {
            return User.GetHashCode();
        }
    }
}