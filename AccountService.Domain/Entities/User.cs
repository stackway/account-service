using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AccountService.Domain.TuneIn;

namespace AccountService.Domain.Entities {
    public class User {
        public long Id { get; set; }

        [EmailAddress] public string Email { get; set; }

        public string PasswordHash { get; set; }
        public string HashSalt { get; set; }

        public string Username { get; set; }

        public ICollection<Topic> Topics { get; set; } = new List<Topic>();
        public ISet<Tag> Tags { get; set; } = new HashSet<Tag>();
        public ICollection<ArchivedResource> ArchivedResources { get; set; } = new List<ArchivedResource>();

        public BlackList BlackList { get; set; }

        public ICollection<CommunityUser> OriginalCommunities { get; set; } = new List<CommunityUser>();
        public ICollection<CommunityUser> RelatedCommunities { get; set; } = new List<CommunityUser>();

        public UserLoginSetup SetupLogin() {
            return new UserLoginSetup(this);
        }

        public UserTopicsSetup SetupTopics() {
            return new UserTopicsSetup(this);
        }

        public UserCommunitiesSetup SetupCommunities() {
            return new UserCommunitiesSetup(this);
        }

        public override bool Equals(object other) {
            if (other == null || GetType() != other.GetType())
                return false;

            return Email == (other as User).Email;
        }

        public override int GetHashCode() {
            return Email.GetHashCode();
        }
    }
}