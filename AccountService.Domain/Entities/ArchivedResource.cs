using System;
using System.Collections.Generic;
using AccountService.Domain.TuneIn;

namespace AccountService.Domain.Entities {
    public class ArchivedResource : Resource {
        public long Id { get; set; }

        public DateTime CreatedAt { get; set; }
        public int UserReview { get; set; }
        public bool IsSaved { get; set; }

        public User User { get; set; }

        public override string DisplayLink { get; set; }
        public override string Link { get; set; }
        public override string Title { get; set; }
        public override string Description { get; set; }
        public override DateTime? PublishedAt { get; set; }
        public override string ImageLink { get; set; }
        public override string Type { get; set; }
        public override double RelevanceIndex { get; set; }
        public override IEnumerable<string> Tags { get; set; } = new List<string>();

        public IEnumerable<string> QueryTags { get; set; } = new List<string>();

        public ArchivedResourceSetup Setup => new ArchivedResourceSetup(this);

        public override bool Equals(object other) {
            if (other == null || GetType() != other.GetType())
                return false;

            return Link == (other as ArchivedResource).Link;
        }

        public override int GetHashCode() {
            return Link.GetHashCode();
        }
    }
}