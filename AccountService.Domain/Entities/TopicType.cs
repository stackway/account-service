using System.Runtime.Serialization;

namespace AccountService.Domain.Entities {
    public enum TopicType {
        [EnumMember(Value = "by-used-tags")]
        BY_USED_TAGS,
        [EnumMember(Value = "by-recently-viewed-tags")]
        BY_RECENTLY_VIEWED_TAGS,
        [EnumMember(Value = "by-similar-users")]
        BY_SIMILAR_USERS,
        [EnumMember(Value = "by-random-tags")]
        BY_RANDOM_TAGS
    }
}