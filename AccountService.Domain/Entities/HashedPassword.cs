namespace AccountService.Domain.Entities {
    public abstract class HashedPassword {
        public string Hash { get; protected set; }
        public string Salt { get; protected set; }

        public override bool Equals(object other) {
            if (other == null || GetType() != other.GetType())
                return false;

            var another = other as HashedPassword;

            return Hash == another.Hash && Salt == another.Salt;
        }

        public override int GetHashCode() {
            return Hash.GetHashCode() + Salt.GetHashCode();
        }
    }
}