using System.Collections.Generic;

using AccountService.Domain.TuneIn;

namespace AccountService.Domain.Entities {
    public class Topic {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TopicType Type { get; set; }

        public User User { get; set; }

        public ICollection<ActualResource> ActualResources { get; set; } = new List<ActualResource>();

        public TopicSetup Setup() {
            return new TopicSetup(this);
        }

        public override bool Equals(object other) {
            if (other == null || GetType() != other.GetType())
                return false;

            var topic = other as Topic;

            return User == topic.User && Type == topic.Type;
        }

        public override int GetHashCode() {
            return User.GetHashCode() + Type.GetHashCode();
        }
    }
}