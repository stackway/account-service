using System.Collections.Generic;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

using Newtonsoft.Json;

namespace AccountService.Domain.Entities {
    public class AccountContext : DbContext {
        public DbSet<User> Users { get; set; }
        public DbSet<CommunityUser> Communities { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<ActualResource> ActualResources { get; set; }
        public DbSet<ArchivedResource> ArchivedResources { get; set; }
        public DbSet<BlackList> BlackLists { get; set; }

        public AccountContext(
            DbContextOptions<AccountContext> options
        ) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder) {
            base.OnModelCreating(builder);

            var topicTypeConverter = new EnumToStringConverter<TopicType>();

            builder.Entity<User>(entity => {
                entity
                    .Property(u => u.Email)
                    .HasMaxLength(120)
                    .IsRequired();

                entity
                    .HasIndex(u => u.Email)
                    .IsUnique();

                entity
                    .Property(u => u.PasswordHash)
                    .IsRequired();

                entity
                    .HasMany(u => u.ArchivedResources)
                    .WithOne(a => a.User);

                entity
                    .HasOne(u => u.BlackList)
                    .WithOne(b => b.User)
                    .HasForeignKey<BlackList>(b => b.UserForeignKey);
            });

            builder.Entity<CommunityUser>(entity => {
                entity
                    .HasOne(c => c.OriginalUser)
                    .WithMany(u => u.OriginalCommunities)
                    .HasForeignKey(c => c.OriginalUserId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity
                    .HasOne(c => c.RelatedUser)
                    .WithMany(u => u.RelatedCommunities)
                    .HasForeignKey(c => c.RelatedUserId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<Topic>(entity => {
                entity
                    .Property(t => t.Name)
                    .HasMaxLength(120)
                    .IsRequired();

                entity
                    .HasOne(t => t.User)
                    .WithMany(t => t.Topics)
                    .OnDelete(DeleteBehavior.Cascade);

                entity
                    .HasMany(t => t.ActualResources)
                    .WithOne(a => a.Topic)
                    .OnDelete(DeleteBehavior.Cascade);

                entity
                    .Property(t => t.Type)
                    .HasConversion(topicTypeConverter);
            });

            builder
                .ApplyConfiguration(new ActualResourceConfig())
                .ApplyConfiguration(new ArchivedResourceConfig());

            builder.Entity<Tag>(entity => {
                entity
                    .Property(t => t.Name)
                    .HasMaxLength(120)
                    .IsRequired();

                entity
                    .HasOne(t => t.User)
                    .WithMany(u => u.Tags)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<IgnoredSource>(entity => {
                entity
                    .HasOne(s => s.BlackList)
                    .WithMany(b => b.Sources)
                    .OnDelete(DeleteBehavior.Cascade);
            });
        }
    }

    public class ArchivedResourceConfig : IEntityTypeConfiguration<ArchivedResource> {
        public void Configure(EntityTypeBuilder<ArchivedResource> builder) {
            var jsonSettings = new JsonSerializerSettings {
                NullValueHandling = NullValueHandling.Ignore
            };

            builder
                .Property(a => a.Tags)
                .HasConversion(
                    value => JsonConvert.SerializeObject(
                        value,
                        jsonSettings
                    ),
                    value => JsonConvert.DeserializeObject<List<string>>(
                        value,
                        jsonSettings
                    )
                );

            builder
                .Property(a => a.QueryTags)
                .HasConversion(
                    value => JsonConvert.SerializeObject(
                        value,
                        jsonSettings
                    ),
                    value => JsonConvert.DeserializeObject<List<string>>(
                        value,
                        jsonSettings
                    )
                );
        }
    }

    public class ActualResourceConfig : IEntityTypeConfiguration<ActualResource> {
        public void Configure(EntityTypeBuilder<ActualResource> builder) {
            var jsonSettings = new JsonSerializerSettings {
                NullValueHandling = NullValueHandling.Ignore
            };

            builder
                .Property(a => a.Tags)
                .HasConversion(
                    value => JsonConvert.SerializeObject(
                        value,
                        jsonSettings
                    ),
                    value => JsonConvert.DeserializeObject<List<string>>(
                        value,
                        jsonSettings
                    )
                );

            builder
                .Property(a => a.QueryTags)
                .HasConversion(
                    value => JsonConvert.SerializeObject(
                        value,
                        jsonSettings
                    ),
                    value => JsonConvert.DeserializeObject<List<string>>(
                        value,
                        jsonSettings
                    )
                );
        }
    }
}