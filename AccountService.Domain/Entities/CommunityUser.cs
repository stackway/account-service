using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccountService.Domain.Entities {
    public class CommunityUser {
        public long Id { get; set; }
        public DateTime CreatedAt { get; set; }

        public long RelatedUserId { get; set; }
        public long OriginalUserId { get; set; }

        public User RelatedUser { get; set; }
        public User OriginalUser { get; set; }
    }
}