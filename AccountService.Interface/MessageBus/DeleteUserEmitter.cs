using AccountService.Application.Services;
using AccountService.Domain.Entities;
using AccountService.Infrastructure.RabbitMQ;
using AutoMapper;

namespace AccountService.Interface.MessageBus {
    public class DeleteUserMessage {
        public long Id { get; set; }
        public string Email { get; set; }
    }

    public class DeleteUserMessageProfile : Profile {
        public DeleteUserMessageProfile() {
            CreateMap<User, DeleteUserMessage>();
        }
    }

    public class DeleteUserMqRoute : RabbitMqSenderRoute<DeleteUserMessage> {
        public DeleteUserMqRoute() {
            Exchange = "delete-user";
            ExchangeType = RabbitMqExchangeType.Fanout;
        }
    }

    public class DeleteUserEmitter {
        private readonly IRabbitMqSender<DeleteUserMessage> _sender;
        private readonly IUserService _service;
        private readonly IMapper _mapper;

        public DeleteUserEmitter(
            IRabbitMqSender<DeleteUserMessage> sender,
            IUserService service,
            IMapper mapper
        ) {
            _sender = sender;
            _service = service;
            _mapper = mapper;
        }

        public void Publish(User user) {
            _sender.Send(_mapper
                .Map<User, DeleteUserMessage>(user)
            );
        }
    }
}