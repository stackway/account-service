using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using AccountService.Domain.Entities;
using AccountService.Application.Services;
using AccountService.Infrastructure.RabbitMQ;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace AccountService.Interface.MessageBus.Consumers {
    public class UserTopicContract {
        public long Id { get; set; }
        public TopicType Type { get; set; }
    }

    public class TopicResourceContract {
        public string Title { get; set; }
        public string Description { get; set; }

        [JsonConverter(typeof(UTCDateTimeConverter))]
        public DateTime? PublishedAt { get; set; }

        public string DisplayLink { get; set; }
        public string Link { get; set; }
        public string ImageLink { get; set; }
        public double RelevanceIndex { get; set; }
        public IEnumerable<String> Tags { get; set; }
        public IEnumerable<String> QueryTags { get; set; }
    }

    public class UTCDateTimeConverter : JsonConverter {
        public override object ReadJson(
            JsonReader reader,
            Type objectType,
            object existingValue,
            JsonSerializer serializer
        ) {
            long unixTime;

            try {
                unixTime = serializer.Deserialize<long>(reader);
            }
            catch (JsonSerializationException) {
                return null;
            }
            catch (NullReferenceException) {
                return null;
            }

            return DateTimeOffset.FromUnixTimeMilliseconds(unixTime).UtcDateTime;
        }

        public override bool CanConvert(Type type) {
            return typeof(DateTime).IsAssignableFrom(type);
        }

        public override void WriteJson(
            JsonWriter writer,
            object value,
            JsonSerializer serializer) {
            throw new NotImplementedException();
        }

        public override bool CanRead {
            get { return true; }
        }
    }

    public class UserResourcesMessage {
        public UserTopicContract Topic { get; set; }
        public IEnumerable<TopicResourceContract> Resources { get; set; }
    }

    public class UserResourcesMessageProfile : Profile {
        public UserResourcesMessageProfile() {
            CreateMap<TopicResourceContract, ActualResource>();
        }
    }

    public class UserResourcesMqRoute : RabbitMqConsumerRoute<UserResourcesMessage> {
        public UserResourcesMqRoute() {
            Queue = "accounts-service.user-resources";
            Exchange = "user-resources";
        }
    }

    public class UpdatedUserResourcesConsumer : IRabbitMqConsumerService<UserResourcesMessage> {
        private readonly ITopicActualResourcesService _service;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public UpdatedUserResourcesConsumer(
            ITopicActualResourcesService service,
            IMapper mapper,
            ILogger<UpdatedUserResourcesConsumer> logger
        ) {
            _service = service;
            _mapper = mapper;
            _logger = logger;
        }

        public void ProcessMessage(UserResourcesMessage message) {
            _logger.LogInformation(
                "update resources for: {topic}",
                message.Topic.Type
            );

            var actualResources = _mapper.Map<IEnumerable<TopicResourceContract>, IEnumerable<ActualResource>>(
                message.Resources
            );

            _service.AddActualResources(
                message.Topic.Id,
                actualResources
            );
        }
    }
}