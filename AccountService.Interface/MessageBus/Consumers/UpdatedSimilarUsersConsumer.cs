using System.Collections.Generic;
using AccountService.Application.Services;
using AccountService.Infrastructure.RabbitMQ;

namespace AccountService.Interface.MessageBus.Consumers {
    public class UpdatedSimilarUsersMessage {
        public long Id { get; set; }
        public string Email { get; set; }

        public IEnumerable<long> SimilarUsers { get; set; }
    }

    public class UpdatedSimilarUsersMqRoute : RabbitMqConsumerRoute<UpdatedSimilarUsersMessage> {
        public UpdatedSimilarUsersMqRoute() {
            Queue = "accounts-service.updated-similar-users";
            Exchange = "updated-similar-users";
        }
    }

    public class UpdatedSimilarUsersConsumer : IRabbitMqConsumerService<UpdatedSimilarUsersMessage> {
        private readonly IUserCommunitiesService _service;

        public UpdatedSimilarUsersConsumer(IUserCommunitiesService service) {
            _service = service;
        }

        public void ProcessMessage(UpdatedSimilarUsersMessage message) {
            _service.AddSimilarUsers(
                message.Id,
                message.SimilarUsers
            );
        }
    }
}