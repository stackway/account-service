using AccountService.Application.Exceptions;
using AccountService.Application.Services;
using AccountService.Domain.Entities;
using AccountService.Infrastructure.RabbitMQ;
using AutoMapper;
using Microsoft.Extensions.Logging;

namespace AccountService.Interface.MessageBus {
    public class ForceUpdateResourcesMessage {
        public long Id { get; set; }
        public string Email { get; set; }
    }

    class ForceUpdateResourcesProfile : Profile {
        public ForceUpdateResourcesProfile() {
            CreateMap<User, ForceUpdateResourcesMessage>();
        }
    }

    public class ForceUpdateResourcesMqRoute : RabbitMqSenderRoute<ForceUpdateResourcesMessage> {
        public ForceUpdateResourcesMqRoute() {
            Exchange = "force-update-resources";
        }
    }

    public class ForceUpdateResourcesEmitter {
        private readonly IRabbitMqSender<ForceUpdateResourcesMessage> _sender;
        private readonly IUserService _userService;
        private readonly IUserTopicsService _topicsService;
        private readonly IUserTagsService _tagsService;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public ForceUpdateResourcesEmitter(
            IRabbitMqSender<ForceUpdateResourcesMessage> sender,
            IUserService userService,
            IUserTopicsService topicsService,
            IUserTagsService tagsService,
            IMapper mapper,
            ILogger<ForceUpdateResourcesEmitter> logger
        ) {
            _sender = sender;
            _userService = userService;
            _topicsService = topicsService;
            _tagsService = tagsService;
            _mapper = mapper;
            _logger = logger;
        }

        public void Publish(long userId) {
            if (_topicsService.HasResources(userId)) {
                _logger.LogInformation(
                    "user has resources: {id}",
                    userId
                );

                return;
            }

            if (!_tagsService.HasTags(userId)) {
                _logger.LogInformation(
                    "user hasn't any tags: {id}",
                    userId
                );

                return;
            }

            User foundUser;

            try {
                foundUser = _userService.FindById(userId);
            }
            catch (EntityNotFoundException<User, long>) {
                return;
            }

            _sender.Send(_mapper
                .Map<User, ForceUpdateResourcesMessage>(foundUser)
            );
        }
    }
}