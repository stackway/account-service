using System;
using System.Collections.Generic;
using AutoMapper;
using AccountService.Domain.Entities;
using AccountService.Infrastructure.RabbitMQ;
using Microsoft.Extensions.Logging;

namespace AccountService.Interface.MessageBus {
    public interface UsedResourceMessage {
        long UserId { get; }
        string UserEmail { get; }

        string Name { get; }
        DateTime ViewedAt { get; }

        IEnumerable<string> Tags { get; }
    }

    public class BaseUsedResourceMessage : UsedResourceMessage {
        public long UserId { get; set; }
        public string UserEmail { get; set; }

        public string Name { get; set; }
        public DateTime ViewedAt { get; set; }

        public IEnumerable<string> Tags { get; set; }

        public BaseUsedResourceMessage Use(User user) {
            UserId = user.Id;
            UserEmail = user.Email;

            return this;
        }

        public BaseUsedResourceMessage ViewAt(DateTime viewTime) {
            ViewedAt = viewTime;

            return this;
        }
    }

    public class UsedResourceMessageProfile : Profile {
        public UsedResourceMessageProfile() {
            CreateMap<ActualResource, BaseUsedResourceMessage>()
                .ForMember(
                    d => d.Name,
                    opt => opt.MapFrom(o => o.Title)
                )
                .ForMember(
                    d => d.Tags,
                    opt => opt.MapFrom(o => o.QueryTags)
                );
        }
    }

    public class UsedResourceMqRoute : RabbitMqSenderRoute<UsedResourceMessage> {
        public UsedResourceMqRoute() {
            Exchange = "used-resource";
        }
    }

    public class UsedResourceEmitter {
        private readonly IRabbitMqSender<UsedResourceMessage> _sender;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public UsedResourceEmitter(
            IRabbitMqSender<UsedResourceMessage> sender,
            IMapper mapper,
            ILogger<UsedResourceEmitter> logger
        ) {
            _sender = sender;
            _mapper = mapper;
            _logger = logger;
        }

        public void Publish(ActualResource resource, User user, DateTime viewedAt) {
            _logger.LogInformation(
                "prepare to publish used resource with query tags: {tags}",
                String.Join(' ', resource.QueryTags)
            );

            _sender.Send(_mapper
                .Map<ActualResource, BaseUsedResourceMessage>(resource)
                .ViewAt(viewedAt)
                .Use(user)
            );
        }

        public void Publish(ActualResource resource, User user) {
            Publish(resource, user, DateTime.Now);
        }
    }
}