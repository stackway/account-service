using Microsoft.Extensions.DependencyInjection;
using AccountService.Infrastructure.RabbitMQ;
using AccountService.Interface.MessageBus.Consumers;

namespace AccountService.Interface.MessageBus {
    public static class MessageBusContext {
        public static IServiceCollection ConfigureMqRoutes(this IServiceCollection services) {
            services
                .AddSingleton<IRabbitMqSenderRoute<UserActivityMessage>, UserActivityMqRoute>()
                .AddSingleton<IRabbitMqSenderRoute<UsedResourceMessage>, UsedResourceMqRoute>()
                .AddSingleton<IRabbitMqSenderRoute<ForceUpdateResourcesMessage>, ForceUpdateResourcesMqRoute>()
                .AddSingleton<IRabbitMqSenderRoute<DeleteUserMessage>, DeleteUserMqRoute>();

            services
                .AddSingleton<IRabbitMqConsumerRoute<UserResourcesMessage>, UserResourcesMqRoute>()
                .AddSingleton<IRabbitMqConsumerRoute<UpdatedSimilarUsersMessage>, UpdatedSimilarUsersMqRoute>();

            return services;
        }

        public static IServiceCollection ConfigureMqServices(this IServiceCollection services) {
            services
                .AddScoped<IRabbitMqConsumerService<UserResourcesMessage>, UpdatedUserResourcesConsumer>()
                .AddScoped<IRabbitMqConsumerService<UpdatedSimilarUsersMessage>, UpdatedSimilarUsersConsumer>();

            return services;
        }

        public static IServiceCollection ConfigureMqSenders(this IServiceCollection services) {
            services
                .AddSingleton<IRabbitMqSender<UserActivityMessage>, RabbitMqSender<UserActivityMessage>>()
                .AddSingleton<IRabbitMqSender<UsedResourceMessage>, RabbitMqSender<UsedResourceMessage>>()
                .AddSingleton<IRabbitMqSender<ForceUpdateResourcesMessage>, RabbitMqSender<ForceUpdateResourcesMessage>>()
                .AddSingleton<IRabbitMqSender<DeleteUserMessage>, RabbitMqSender<DeleteUserMessage>>();

            services
                .AddScoped<UserActivityEmitter>()
                .AddScoped<UsedResourceEmitter>()
                .AddScoped<ForceUpdateResourcesEmitter>()
                .AddScoped<DeleteUserEmitter>();

            return services;
        }

        public static IServiceCollection ConfigureMqConsumers(this IServiceCollection services) {
            services
                .AddScoped<
                    IRabbitMqConsumer<UserResourcesMessage>,
                    RabbitMqConsumer<UserResourcesMessage>
                >()
                .AddScoped<
                    IRabbitMqConsumer<UpdatedSimilarUsersMessage>,
                    RabbitMqConsumer<UpdatedSimilarUsersMessage>
                >();

            return services;
        }

        public static IServiceCollection ConfigureMqHostedServices(this IServiceCollection services) {
            services
                .AddHostedService<RabbitMqConsumerHostedService<UserResourcesMessage>>()
                .AddHostedService<RabbitMqConsumerHostedService<UpdatedSimilarUsersMessage>>();

            return services;
        }
    }
}