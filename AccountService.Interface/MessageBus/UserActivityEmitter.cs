using System;
using AutoMapper;
using AccountService.Domain.Entities;
using AccountService.Application.Services;
using AccountService.Application.Exceptions;
using AccountService.Infrastructure.RabbitMQ;

namespace AccountService.Interface.MessageBus {
    public interface UserActivityMessage {
        long Id { get; }
        string Email { get; }
        DateTime ActivedAt { get; }
    }

    public class BaseActivityMessage : UserActivityMessage {
        public long Id { get; set; }

        public string Email { get; set; }

        public DateTime ActivedAt { get; set; }

        public BaseActivityMessage ActiveAt(DateTime activeTime) {
            ActivedAt = activeTime;

            return this;
        }
    }

    public class UserActivityMessageProfile : Profile {
        public UserActivityMessageProfile() {
            CreateMap<User, BaseActivityMessage>();
        }
    }

    public class UserActivityMqRoute : RabbitMqSenderRoute<UserActivityMessage> {
        public UserActivityMqRoute() {
            Exchange = "user-activity";
        }
    }

    public class UserActivityEmitter {
        private readonly IRabbitMqSender<UserActivityMessage> _sender;
        private readonly IUserService _service;
        private readonly IMapper _mapper;

        public UserActivityEmitter(
            IRabbitMqSender<UserActivityMessage> sender,
            IUserService service,
            IMapper mapper
        ) {
            _sender = sender;
            _service = service;
            _mapper = mapper;
        }

        public void Publish(User user, DateTime activedAt) {
            _sender.Send(_mapper
                .Map<User, BaseActivityMessage>(user)
                .ActiveAt(activedAt)
            );
        }

        public void Publish(User user) {
            Publish(user, DateTime.Now);
        }

        public void Publish(long userId, DateTime activedAt) {
            User foundUser;

            try {
                foundUser = _service.FindById(userId);
            }
            catch (EntityNotFoundException<User, long>) {
                return;
            }

            _sender.Send(_mapper
                .Map<User, BaseActivityMessage>(foundUser)
                .ActiveAt(activedAt)
            );
        }

        public void Publish(long userId) {
            Publish(userId, DateTime.Now);
        }
    }
}