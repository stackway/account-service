using AccountService.Application.Exceptions;
using AccountService.Application.Services;
using AccountService.Domain.Entities;
using AccountService.Interface.MessageBus;
using Microsoft.Extensions.Logging;

namespace AccountService.Interface.Services {
    public interface INotificationService {
        void SendResource(long userId, long topicId, long resourceId);
        void SendActivity(long userId);
        void SendActivity(User user);
        void RemoveUser(User user);
    }

    public class NotificationService : INotificationService {
        private readonly IUserService _userService;
        private readonly ITopicActualResourcesService _resourcesService;
        private readonly UsedResourceEmitter _usedResourceEmitter;
        private readonly UserActivityEmitter _activityEmitter;
        private readonly DeleteUserEmitter _deleteUserEmitter;
        private readonly ILogger _logger;

        public NotificationService(
            IUserService userService,
            ITopicActualResourcesService resourcesService,
            UsedResourceEmitter usedResourceEmitter,
            UserActivityEmitter activityEmitter,
            DeleteUserEmitter deleteUserEmitter,
            ILogger<NotificationService> logger
        ) {
            _userService = userService;
            _resourcesService = resourcesService;
            _usedResourceEmitter = usedResourceEmitter;
            _activityEmitter = activityEmitter;
            _deleteUserEmitter = deleteUserEmitter;
            _logger = logger;
        }

        public void SendResource(long userId, long topicId, long resourceId) {
            try {
                _usedResourceEmitter.Publish(
                    _resourcesService
                        .FindActualResource(topicId, resourceId),
                    _userService.FindById(userId)
                );
            }
            catch (EntityNotFoundException<User, long> exception) {
                _logger.LogWarning(exception.Message);
            }
            catch (EntityNotFoundException<ActualResource, long> exception) {
                _logger.LogWarning(exception.Message);
            }
        }

        public void SendActivity(long userId) {
            try {
                _activityEmitter.Publish(_userService.FindById(userId));
            }
            catch (EntityNotFoundException<User, long> exception) {
                _logger.LogWarning(exception.Message);
            }
        }

        public void SendActivity(User user) {
            _activityEmitter.Publish(user);
        }

        public void RemoveUser(User user) {
            _deleteUserEmitter.Publish(user);
        }
    }
}