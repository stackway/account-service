using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using AutoMapper;
using AccountService.Application.Services;
using AccountService.Application.Units;
using AccountService.Domain.Entities;
using AccountService.Domain.Units;
using AccountService.Infrastructure.DataBase;
using AccountService.Infrastructure.RabbitMQ;
using AccountService.Infrastructure.Security;
using AccountService.Interface.MessageBus;
using AccountService.Interface.Services;

namespace AccountService.Interface {
    public class Startup {
        public Startup(IConfiguration configuration, ILoggerFactory loggerFactory) {
            Configuration = configuration;
            LoggerFactory = loggerFactory;
        }

        public IConfiguration Configuration { get; }
        public ILoggerFactory LoggerFactory { get; }

        public void ConfigureServices(IServiceCollection services) {
            services.AddHealthChecks();

            services.AddCors(options => {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services
                .ConfigureBearerTokensOptions(Configuration)
                .ConfigureAuth(Configuration);

            services.ConfigureDbContext<AccountContext>(Configuration);

            services.AddScoped<IPrepareUserWorkspaceUnit, BaseWorkspaceStarter>();

            services
                .AddScoped<IUserService, UserService>()
                .AddScoped<IUserTokenService, UserTokenService>()
                .AddScoped<IUserTopicsService, UserTopicsService>()
                .AddScoped<IUserTagsService, UserTagsService>()
                .AddScoped<ITopicsService, TopicsService>()
                .AddScoped<ITagsService, TagsService>()
                .AddScoped<ITopicActualResourcesService, TopicActualResourcesService>()
                .AddScoped<IUserArchivedResourcesService, UserArchivedResourcesService>()
                .AddScoped<IUserCommunitiesService, UserCommunitiesService>()
                .AddScoped<INotificationService, NotificationService>();

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.ConfigureRabbitMqConnection(Configuration);

            services
                .ConfigureMqRoutes()
                .ConfigureMqServices()
                .ConfigureMqSenders()
                .ConfigureMqConsumers()
                .ConfigureMqHostedServices();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }
            else {
                app.UseHsts();
            }

            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseCors("CorsPolicy");
            app.UseMvc();

            app.ApplicationServices.UseMigrations<AccountContext>();

            app.UseHealthChecks(
                "/health",
                new HealthCheckOptions {
                    Predicate = check => check.Tags.Contains("ready")
                }
            );
        }
    }
}