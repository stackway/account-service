using System;

using Microsoft.AspNetCore.Mvc;

using AccountService.Infrastructure.HttpContext;

namespace AccountService.Interface.Resources {
    public abstract class BaseController : Controller {
        public bool IsAuthorizedRequestById(long userId) {
            try {
                if (this.HttpContext.User.GetUserId() != userId)
                    return false;
            }
            catch (ArgumentException) {
                return false;
            }

            return true;
        }

        public bool IsAuthorizedRequestByEmail(string email) {
            try {
                if (this.HttpContext.User.GetUserEmail() != email)
                    return false;
            }
            catch (ArgumentException) {
                return false;
            }

            return true;
        }

        public long? GetUserId() {
            try {
                return this.HttpContext.User.GetUserId();
            }
            catch (ArgumentException) {
                return null;
            }
        }
    }
}