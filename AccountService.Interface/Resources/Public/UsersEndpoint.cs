using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using AccountService.Domain.Entities;
using AccountService.Application.Contracts;
using AccountService.Application.Services;
using AccountService.Application.Exceptions;
using AccountService.Interface.Services;

namespace AccountService.Interface.Resources.Public {
    [Route("public/api/users")]
    [EnableCors("CorsPolicy")]
    [Authorize]
    public class UsersEndpoint : BaseController {
        private readonly IUserService _userService;
        private readonly INotificationService _notificationService;

        public UsersEndpoint(
            IUserService userService,
            INotificationService notificationService
        ) {
            _userService = userService;
            _notificationService = notificationService;
        }

        [HttpGet("{id}")]
        public IActionResult GetUserById([FromRoute] long id) {
            if (!IsAuthorizedRequestById(id)) {
                return Challenge();
            }

            User foundUser;

            try {
                foundUser = _userService.FindById(id);
            }
            catch (EntityNotFoundException<User, long>) {
                return NotFound();
            }

            return Json(new UserInfo(foundUser));
        }

        [HttpGet("find")]
        public IActionResult GetUserByEmail([FromQuery] string email) {
            if (!IsAuthorizedRequestByEmail(email))
                return Challenge();

            User foundUser;

            try {
                foundUser = _userService.FindByEmail(email);
            }
            catch (EntityNotFoundException<User, string>) {
                return NotFound();
            }

            _notificationService.SendActivity(foundUser);

            return Json(new UserInfo(foundUser));
        }

        [AllowAnonymous, HttpPost]
        public IActionResult AddUser([FromBody] RegisterModel model) {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.Errors());

            try {
                _userService.AddUser(new UserFromRegister(model));
            }
            catch (EntityExistException<User, long> exception) {
                return BadRequest(exception.Message);
            }

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteUser([FromRoute] long id) {
            if (!IsAuthorizedRequestById(id)) {
                return Challenge();
            }

            try {
                var removedUser = _userService.RemoveUser(id);

                _notificationService.RemoveUser(removedUser);
            }
            catch (EntityNotFoundException<User, long>) {
                return NotFound();
            }

            return Ok();
        }
    }
}