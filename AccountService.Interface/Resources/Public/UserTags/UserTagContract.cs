using System;
using System.Collections.Generic;
using System.Linq;
using AccountService.Domain.Entities;

namespace AccountService.Interface.Resources.Public.UserTags {
    public class UserTagContract {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Views { get; set; }
        public DateTime RecentlyUsed { get; set; }
        public int Importance { get; set; }
        public bool IsHidden { get; set; }
    }
}