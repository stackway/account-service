using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using AutoMapper;

using AccountService.Domain.Entities;
using AccountService.Application.Services;
using AccountService.Application.Exceptions;

namespace AccountService.Interface.Resources.Public.UserTags {
    [Route("public/api/users/{user-id}/tags")]
    [EnableCors("CorsPolicy")]
    [Authorize]
    public class UserTagsEndpoint : BaseController {
        private readonly IUserTagsService _service;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public UserTagsEndpoint(
            IUserTagsService service,
            IMapper mapper,
            ILogger<UserTagsEndpoint> logger
        ) {
            _service = service;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult GetTags([FromRoute(Name = "user-id")] long userId) {
            if (!IsAuthorizedRequestById(userId)) {
                return Challenge();
            }

            return Json(_mapper
                .Map<IEnumerable<Tag>, IEnumerable<UserTagContract>>(
                    _service.FindTags(userId)
                )
            );
        }

        [HttpGet("{tag-id}")]
        public IActionResult GetTag(
            [FromRoute(Name = "user-id")] long userId,
            [FromRoute(Name = "tag-id")] long tagId
        ) {
            if (!IsAuthorizedRequestById(userId)) {
                return Challenge();
            }

            try {
                return Json(
                    _mapper.Map<Tag, UserTagContract>(
                        _service.FindTag(userId, tagId)
                ));
            }
            catch (EntityNotFoundException<Tag, long> exception) {
                _logger.LogWarning(exception.Message);

                return NotFound();
            }
        }

        [HttpPost]
        public IActionResult AddTag(
            [FromRoute(Name = "user-id")] long userId,
            [FromBody] UserTagContract tag
        ) {
            if (!IsAuthorizedRequestById(userId)) {
                return Challenge();
            }

            if (!ModelState.IsValid) {
                return BadRequest();
            }

            try {
                _service.AddTag(userId, _mapper.Map<UserTagContract, Tag>(tag));
            }
            catch (EntityExistException<Tag, long> exception) {
                _logger.LogWarning(exception.Message);

                return BadRequest();
            }
            catch (EntityNotFoundException<User, long> exception) {
                _logger.LogWarning(exception.Message);

                return BadRequest();
            }

            return Ok();
        }

        [HttpDelete("{tag-id}")]
        public IActionResult RemoveTag(
            [FromRoute(Name = "user-id")] long userId,
            [FromRoute(Name = "tag-id")] long tagId
        ) {
            if (!IsAuthorizedRequestById(userId)) {
                return Challenge();
            }

            try {
                _service.RemoveTag(userId, tagId);
            }
            catch (EntityNotFoundException<Tag, long> exception) {
                _logger.LogWarning(exception.Message);

                return NotFound(exception.Message);
            }

            return Ok();
        }
    }
}