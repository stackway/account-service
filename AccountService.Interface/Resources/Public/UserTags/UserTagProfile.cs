using AutoMapper;

using AccountService.Domain.Entities;

namespace AccountService.Interface.Resources.Public.UserTags {
    public class UserTagProfile : Profile {
        public UserTagProfile() {
            CreateMap<Tag, UserTagContract>();
            CreateMap<UserTagContract, Tag>();
        }
    }
}