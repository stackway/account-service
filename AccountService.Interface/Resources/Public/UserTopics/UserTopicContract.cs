using System.Collections.Generic;

namespace AccountService.Interface.Resources.Public.UserTopics {
    public class UserTopicContract {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public IEnumerable<TopicResourceContract> ActualResources { get; set; }

        public UserTopicContract UseResources(
            IEnumerable<TopicResourceContract> resources
        ) {
            this.ActualResources = resources;

            return this;
        }
    }
}