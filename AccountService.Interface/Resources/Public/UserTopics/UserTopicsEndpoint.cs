using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AutoMapper;
using AccountService.Domain.Entities;
using AccountService.Infrastructure.HttpContext;
using AccountService.Application.Contracts;
using AccountService.Application.Services;
using AccountService.Application.Exceptions;
using AccountService.Interface.MessageBus;
using AccountService.Interface.Resources;

namespace AccountService.Interface.Resources.Public.UserTopics {
    [Route("public/api/users/{user-id}/topics")]
    [EnableCors("CorsPolicy")]
    [Authorize]
    public class UserTopicsEndpoint : BaseController {
        private readonly IUserTopicsService _service;
        private readonly ForceUpdateResourcesEmitter _emitter;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public UserTopicsEndpoint(
            IUserTopicsService service,
            ForceUpdateResourcesEmitter emitter,
            IMapper mapper,
            ILogger<UserTopicsEndpoint> logger
        ) {
            _service = service;
            _emitter = emitter;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult GetTopics([FromRoute(Name = "user-id")] long userId) {
            if (!IsAuthorizedRequestById(userId)) {
                return Challenge();
            }

            var topics = _service.FindTopics(userId);

            var contracts = topics.Select(topic => _mapper
                .Map<Topic, UserTopicContract>(topic)
                .UseResources(_mapper
                    .Map<IEnumerable<ActualResource>, IEnumerable<TopicResourceContract>>(
                        topic.ActualResources
                    ))
            );

            return Json(contracts);
        }

        [HttpGet("{topic-id}")]
        public IActionResult GetTopic(
            [FromRoute(Name = "user-id")] long userId,
            [FromRoute(Name = "topic-id")] long topicId
        ) {
            if (!IsAuthorizedRequestById(userId))
                return Challenge();

            try {
                var foundTopic = _service.FindTopic(userId, topicId);

                return Json(_mapper
                    .Map<Topic, UserTopicContract>(foundTopic)
                    .UseResources(_mapper
                        .Map<IEnumerable<ActualResource>, IEnumerable<TopicResourceContract>>(
                            foundTopic.ActualResources
                        ))
                );
            }
            catch (EntityNotFoundException<Topic, long> exception) {
                _logger.LogWarning(exception.Message);

                return NotFound();
            }
        }

        [HttpPost]
        public IActionResult AddTopic(
            [FromRoute(Name = "user-id")] long userId,
            [FromBody] TopicModel topic
        ) {
            if (!IsAuthorizedRequestById(userId)) {
                return Challenge();
            }

            if (!ModelState.IsValid) {
                return BadRequest();
            }

            try {
                _service.AddTopic(userId, new TopicFromModel(topic));
            }
            catch (EntityExistException<Topic, long> exception) {
                _logger.LogWarning(exception.Message);

                return BadRequest();
            }
            catch (EntityNotFoundException<User, long> exception) {
                _logger.LogWarning(exception.Message);

                return BadRequest();
            }

            return Ok();
        }

        [HttpDelete("{topic-id}")]
        public IActionResult DeleteTopic(
            [FromRoute(Name = "user-id")] long userId,
            [FromRoute(Name = "topic-id")] long topicId
        ) {
            if (!IsAuthorizedRequestById(userId))
                return Challenge();

            try {
                _service.RemoveTopic(userId, topicId);
            }
            catch (EntityNotFoundException<Topic, long> exception) {
                _logger.LogWarning(exception.Message);

                return NotFound(exception.Message);
            }

            return Ok();
        }


        [HttpPost("force-update")]
        public IActionResult ForceUpdateResources(
            [FromRoute(Name = "user-id")] long userId
        ) {
            _emitter.Publish(userId);

            return Ok();
        }
    }
}