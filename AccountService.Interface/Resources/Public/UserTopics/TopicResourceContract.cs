using System;
using System.Collections.Generic;

namespace AccountService.Interface.Resources.Public.UserTopics {
    public class TopicResourceContract {
        public long Id { get; set; }
        public string DisplayLink { get; set; }
        public string Link { get; set; }
        public string ImageLink { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime PublishedAt { get; set; }
        public double RelevanceIndex { get; set; }
        public IEnumerable<string> Tags { get; set; }

        public IEnumerable<string> QueryTags { get; set; }
    }
}