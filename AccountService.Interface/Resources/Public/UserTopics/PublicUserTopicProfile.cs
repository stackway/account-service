using AutoMapper;

using AccountService.Domain.Entities;

namespace AccountService.Interface.Resources.Public.UserTopics {
    public class PublicUserTopicProfile : Profile {
        public PublicUserTopicProfile() {
            CreateMap<Topic, UserTopicContract>();

            CreateMap<ActualResource, TopicResourceContract>();
        }
    }
}