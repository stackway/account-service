using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AccountService.Domain.Entities;
using AccountService.Application.Services;
using AccountService.Application.Exceptions;
using AccountService.Interface.Services;

namespace AccountService.Interface.Resources.Public.TopicActualResources {
    [Route("public/api/topics/{topic-id}/actual-resources")]
    [EnableCors("CorsPolicy")]
    [Authorize]
    public class TopicActualResourcesEndpoint : BaseController {
        private readonly ITopicActualResourcesService _actualResourcesService;
        private readonly INotificationService _notificationService;
        private readonly ILogger _logger;

        public TopicActualResourcesEndpoint(
            ITopicActualResourcesService actualResourcesService,
            ILogger<TopicActualResourcesEndpoint> logger,
            INotificationService notificationService
        ) {
            _actualResourcesService = actualResourcesService;
            _logger = logger;
            _notificationService = notificationService;
        }

        [HttpPost("{resource-id}")]
        public IActionResult ArchiveResource(
            [FromRoute(Name = "topic-id")] long topicId,
            [FromRoute(Name = "resource-id")] long resourceId
        ) {
            try {
                var userId = GetUserId();

                if (userId.HasValue) {
                    _notificationService.SendResource(
                        userId.Value,
                        topicId,
                        resourceId
                    );
                }

                _actualResourcesService.ArchiveActualResource(topicId, resourceId);

                return Ok();
            }
            catch (EntityNotFoundException<Topic, long> exception) {
                _logger.LogWarning(exception.Message);

                return NotFound();
            }
        }
    }
}