using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using AccountService.Domain.Entities;
using AccountService.Application.Services;
using AccountService.Application.Exceptions;

namespace AccountService.Interface.Resources.Public {
    [Route("public/api/tags")]
    [EnableCors("CorsPolicy")]
    [Authorize]
    public class TagsEndpoint : BaseController {
        private readonly ITagsService _service;
        private readonly ILogger _logger;

        public TagsEndpoint(
            ITagsService service,
            ILogger<TagsEndpoint> logger
        ) {
            _service = service;
            _logger = logger;
        }

        [HttpPut("{id}")]
        public IActionResult UpdateTag([FromRoute] long id, [FromBody] Tag tag) {
            try {
                _service.UpdateTag(id, tag);
                return Ok();
            }
            catch (EntityNotFoundException<Tag, long> exception) {
                _logger.LogWarning(exception.Message);

                return NotFound();
            }
        }
    }
}