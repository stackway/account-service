using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using AccountService.Domain.Entities;
using AccountService.Application.Services;
using AccountService.Application.Exceptions;
using AccountService.Interface.Resources;

namespace AccountService.Interface.Resources.Public {
    [Route("public/api/topics")]
    [EnableCors("CorsPolicy")]
    [Authorize]
    public class TopicsEndpoint : BaseController {
        private ITopicsService _service;
        private ILogger _logger;

        public TopicsEndpoint(
            ITopicsService service,
            ILogger<TopicsEndpoint> logger
        ) {
            _service = service;
            _logger = logger;
        }

        [HttpPut("{id}")]
        public IActionResult UpdateTopic([FromRoute] long id, [FromBody] Topic topic) {
            try {
                _service.UpdateTopic(id, topic);

                return Ok();
            }
            catch (EntityNotFoundException<Topic, long> exception) {
                _logger.LogWarning(exception.Message);

                return NotFound();
            }
        }
    }
}