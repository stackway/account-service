using System;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using AccountService.Application.Contracts;
using AccountService.Application.Services;
using AccountService.Application.Exceptions;
using AccountService.Domain.Entities;

namespace AccountService.Interface.Resources.Public {
    [Route("public/api/users/token")]
    [EnableCors("CorsPolicy")]
    public class UserTokenEndpoint : Controller {
        private readonly IUserTokenService _service;
        private readonly ILogger _logger;

        public UserTokenEndpoint(
            IUserTokenService service,
            ILogger<UserTokenEndpoint> logger
        ) {
            _service = service;
            _logger = logger;
        }

        [AllowAnonymous, HttpGet]
        public IActionResult GetToken([FromQuery] LoginModel login) {
            if (!ModelState.IsValid) {
                _logger.LogInformation(login.ToString());

                return BadRequest(ModelState.Errors());
            }

            try {
                return Json(_service.GetToken(login));
            }
            catch (ArgumentException exception) {
                _logger.LogInformation(exception.Message);

                return BadRequest(exception.Message);
            }
            catch (EntityNotFoundException<User, string> exception) {
                _logger.LogInformation(exception.Message);

                return BadRequest(exception.Message);
            }
        }

        [Authorize, HttpGet("is-valid")]
        public IActionResult IsValid() {
            return Json(new { IsValid = true });
        }
    }
}