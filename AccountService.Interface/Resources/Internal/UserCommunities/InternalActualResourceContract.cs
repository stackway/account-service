using System;
using System.Collections.Generic;

namespace AccountService.Interface.Resources.Internal.UserCommunities {
    public class InternalActualResourceContract {
        public string Title { get; set; }
        public string Description { get; set; }

        public DateTime? PublishedAt { get; set; }

        public string DisplayLink { get; set; }
        public string Link { get; set; }
        public string ImageLink { get; set; }
        public double RelevanceIndex { get; set; }
        public IEnumerable<String> Tags { get; set; }
        public IEnumerable<String> QueryTags { get; set; }
    }
}