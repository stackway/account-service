using AccountService.Domain.Entities;
using AutoMapper;

namespace AccountService.Interface.Resources.Internal.UserCommunities {
    public class InternalActualResourceProfile : Profile {
        public InternalActualResourceProfile() {
            CreateMap<ArchivedResource, InternalActualResourceContract>();
        }
    }
}