using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using AccountService.Application.Services;
using AccountService.Domain.Entities;
using AutoMapper;

namespace AccountService.Interface.Resources.Internal.UserCommunities {
    [Route("internal/api/users/{user-id}/communities")]
    [EnableCors("CorsPolicy")]
    [AllowAnonymous]
    public class InternalUserCommunitiesEndpoint : Controller {
        private readonly IUserCommunitiesService _service;
        private readonly IMapper _mapper;

        public InternalUserCommunitiesEndpoint(
            IUserCommunitiesService service,
            IMapper mapper
        ) {
            _service = service;
            _mapper = mapper;
        }

        [HttpGet("resources")]
        public IActionResult GetUnique(
            [FromRoute(Name = "user-id")] long userId,
            [FromQuery] int takeFromUser
        ) {
            return Json(
                _mapper.Map<IEnumerable<ArchivedResource>, IEnumerable<InternalActualResourceContract>>(
                    _service.GetResources(userId, takeFromUser)
                )
            );
        }
    }
}