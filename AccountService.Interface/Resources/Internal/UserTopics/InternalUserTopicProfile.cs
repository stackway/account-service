using AutoMapper;

using AccountService.Domain.Entities;

namespace AccountService.Interface.Resources.Internal.UserTopics {
    public class InternalUserTopicProfiler : Profile {
        public InternalUserTopicProfiler() {
            CreateMap<Topic, InternalUserTopicContract>();
        }
    }
}