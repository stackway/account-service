using AccountService.Domain.Entities;

namespace AccountService.Interface.Resources.Internal.UserTopics {
    public class InternalUserTopicContract {
        public long Id { get; set; }
        public string Name { get; set; }
        public TopicType Type { get; set; }
    }
}