using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

using AccountService.Application.Services;
using AutoMapper;
using System.Linq;
using System.Collections.Generic;
using AccountService.Domain.Entities;

namespace AccountService.Interface.Resources.Internal.UserTopics {
    [Route("internal/api/users/{user-id}/topics")]
    [EnableCors("CorsPolicy")]
    [AllowAnonymous]
    public class InternalUserTopicsEndpoint : Controller {
        private readonly IUserTopicsService _service;
        private readonly IMapper _mapper;

        public InternalUserTopicsEndpoint(
            IUserTopicsService service,
            IMapper mapper
        ) {
            _service = service;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetTopics([FromRoute(Name = "user-id")] long userId) {
            var contracts = _mapper.Map<IEnumerable<Topic>, IEnumerable<InternalUserTopicContract>>(
                _service.FindTopics(userId)
            );

            return Json(contracts);
        }
    }
}