using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

using AccountService.Application.Services;
using AutoMapper;
using System.Linq;
using System.Collections.Generic;
using AccountService.Domain.Entities;

namespace AccountService.Interface.Resources.Internal.UserTags {
    [Route("internal/api/users/{user-id}/tags")]
    [EnableCors("CorsPolicy")]
    [AllowAnonymous]
    public class InternalUserTagsEndpoint : Controller {
        private readonly IUserTagsService _service;
        private readonly IMapper _mapper;

        public InternalUserTagsEndpoint(
            IUserTagsService service,
            IMapper mapper
        ) {
            _service = service;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAllTags([FromRoute(Name = "user-id")] long userId) {
            var contracts = _mapper.Map<IEnumerable<Tag>, IEnumerable<InternalUserTagContract>>(
                _service.FindAviableTags(userId)
            );

            return Json(contracts);
        }
    }
}