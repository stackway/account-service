using AutoMapper;

using AccountService.Domain.Entities;
using System.Linq;

namespace AccountService.Interface.Resources.Internal.UserTags {
    public class InternalUserTagProfile : Profile {
        public InternalUserTagProfile() {
            CreateMap<Tag, InternalUserTagContract>();
        }
    }
}