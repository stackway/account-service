using System;
using System.Collections.Generic;

namespace AccountService.Interface.Resources.Internal.UserTags {
    public class InternalUserTagContract {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Views { get; set; }
        public DateTime RecentlyUsed { get; set; }
        public int Importance { get; set; }
    }
}