using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

using AccountService.Application.Services;
using AccountService.Domain.Entities;

using System.Linq;
using System.Collections.Generic;


namespace AccountService.Interface.Resources.Internal.UserResources {
    [Route("internal/api/users/{user-id}/resources")]
    [EnableCors("CorsPolicy")]
    [AllowAnonymous]
    public class InternalUserResourcesEndpoint : Controller {
        private readonly IUserArchivedResourcesService _service;

        public InternalUserResourcesEndpoint(
            IUserArchivedResourcesService service
        ) {
            _service = service;
        }

        [HttpPost("filter-unique")]
        public IActionResult GetUnique(
            [FromRoute(Name = "user-id")] long userId,
            [FromBody] IEnumerable<string> links
        ) {
            return Json(_service.GetUnique(userId, links));
        }
    }
}