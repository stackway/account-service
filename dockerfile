# Stage 1
FROM microsoft/dotnet:2.2-sdk AS builder
WORKDIR /source

# caches restore result by copying csproj files separately
COPY *.sln .
COPY **/*.Domain.csproj ./AccountService.Domain/AccountService.Domain.csproj
COPY **/*.Infrastructure.csproj ./AccountService.Infrastructure/AccountService.Infrastructure.csproj
COPY **/*.Application.csproj ./AccountService.Application/AccountService.Application.csproj
COPY **/*.Interface.csproj ./AccountService.Interface/AccountService.Interface.csproj
RUN dotnet restore *.sln

# copies the rest of code
COPY . .
RUN dotnet publish --output /app/ --configuration Release

COPY ./scripts/ /app/scripts/
COPY ./docker-entrypoint.sh /app/docker-entrypoint.sh

# Stage 2
FROM microsoft/dotnet:2.2-aspnetcore-runtime
WORKDIR /app
COPY --from=builder /app .
ENTRYPOINT ["./docker-entrypoint.sh"]