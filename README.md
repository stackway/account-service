# AccountService

## Resources

- <https://github.com/quozd/awesome-dotnet>
- <https://github.com/thangchung/awesome-dotnet-core>

### Authentication

- <https://github.com/VahidN/ASPNETCore2JwtAuthentication>

- <https://metanit.com/sharp/aspnet5/23.7.php>

- <https://andrey.moveax.ru/post/asp-net-core-web-api-authentication-part-1-jwt>

### Hashing

- <https://docs.microsoft.com/en-us/aspnet/core/security/data-protection/consumer-apis/password-hashing?view=aspnetcore-2.1>

- <https://www.c-sharpcorner.com/article/hashing-passwords-in-net-core-with-tips/>

### Infrastructure

- <https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-2.1#environment-variables-configuration-provider>

- <https://github.com/Biarity/Sieve>
- <https://github.com/hherzl/EntityFrameworkCore2ForEnterprise>
- <https://github.com/trenoncourt/AutoQueryable>

- <https://github.com/EminemJK/Banana>

- <https://stackoverflow.com/questions/37276932/what-the-difference-between-fromroute-and-frombody-in-web-api>

