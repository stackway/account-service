using System;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace AccountService.Infrastructure.DataBase {
    public static class DbContextConfiguration {
        public static IServiceCollection ConfigureDbContext<T>(
            this IServiceCollection services,
            IConfiguration config
        ) where T : DbContext {
            Action<DbContextOptionsBuilder> contextBuilder =
                options => options.UseMySql(config.GetConnectionString("DEFAULT"));

            if (config.GetValue("DB:USE_IN_MEMORY", true))
                contextBuilder = options => options.UseInMemoryDatabase("InMemoryDb");

            services.AddDbContext<T>(contextBuilder);

            return services;
        }

        public static IServiceProvider UseMigrations<T>(
            this IServiceProvider services
        ) where T : DbContext {
            using (var serviceScope = services.CreateScope()) {
                var context = serviceScope.ServiceProvider.GetService<T>();

                if (context.Database.IsInMemory())
                    return services;

                context.Database.Migrate();

                try {
                    context.GetService<IRelationalDatabaseCreator>().CreateTables();
                }
                catch (Exception exception) {
                    Console.WriteLine(exception.Message);
                }
            }

            return services;
        }
    }
}
