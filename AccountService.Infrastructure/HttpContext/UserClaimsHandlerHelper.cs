using System;
using System.Security.Claims;

using AccountService.Infrastructure.Security;

namespace AccountService.Infrastructure.HttpContext {
    public static class UserClaimsHandlerHelper {
        public static long GetUserId(this ClaimsPrincipal claims) {
            var claimWithUserId = claims.FindFirst(UserClaimsTypes.Id);

            if (claimWithUserId == null)
                throw new ArgumentException("user doesn't have id");

            return long.Parse(claimWithUserId.Value);
        }

        public static string GetUserEmail(this ClaimsPrincipal claims) {
            return claims.Identity.Name;
        }
    }
}