using System;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace AccountService.Infrastructure.Security {
    public static class AuthConfiguration {
        public static IServiceCollection ConfigureAuth(
            this IServiceCollection services,
            IConfiguration config
        ) {
            var tokenOptions = BearerTokensOptions.FromConfiguration(config);

            services
                .AddAuthentication(options => {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(
                    JwtBearerDefaults.AuthenticationScheme,
                    options => {
                        options.RequireHttpsMetadata = true;
                        options.TokenValidationParameters = new TokenValidationParameters {
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = tokenOptions.GetSymmetricSignSecurityKey(),

                            TokenDecryptionKey = tokenOptions.GetSymmetricEncryptionKey(),

                            ValidateIssuer = true,
                            ValidIssuer = tokenOptions.Issuer,

                            ValidateLifetime = true,
                            ClockSkew = TimeSpan.FromMinutes(5),

                            ValidateAudience = false,
                        };
                    });

            return services;
        }

        public static IServiceCollection ConfigureBearerTokensOptions(
            this IServiceCollection services,
            IConfiguration config
        ) {
            services.AddTransient<BearerTokensOptions>(
                _ => BearerTokensOptions.FromConfiguration(config)
            );

            return services;
        }
    }
}