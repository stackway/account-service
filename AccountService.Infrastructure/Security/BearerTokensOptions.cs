using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace AccountService.Infrastructure.Security {
    public class BearerTokensOptions {
        /// <summary>
        /// service who provide the token
        /// </summary>
        public readonly string Issuer;

        public readonly string Audience;

        /// <summary>
        /// key to encode the token
        /// </summary>
        private readonly string _signKey;

        /// <summary>
        /// key to encrypt the token payload
        /// </summary>
        private readonly string _encryptKey;

        /// <summary>
        /// time to life in hours
        /// </summary>
        public readonly int AccessTokenExpirationHours;

        public BearerTokensOptions(
            string issuer,
            string audience,
            string signKey,
            string encryptKey,
            int expirationHours
        ) {
            Issuer = issuer;
            Audience = audience;
            _signKey = signKey;
            _encryptKey = encryptKey;
            AccessTokenExpirationHours = expirationHours;
        }

        public static BearerTokensOptions FromConfiguration(IConfiguration config) {
            return new BearerTokensOptions(
                config["Auth:JWT:Issuer"],
                config["Auth:JWT:Audience"],
                config["Auth:JWT:SingKey"],
                config["Auth:JWT:EncryptKey"],
                config.GetValue<int>("Auth:JWT:Lifetime")
            );
        }

        public SymmetricSecurityKey GetSymmetricSignSecurityKey() {
            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_signKey));
        }

        public SymmetricSecurityKey GetSymmetricEncryptionKey() {
            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_encryptKey));
        }
    }
}