using System;
using System.Security.Cryptography;
using AccountService.Domain.Entities;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace AccountService.Infrastructure.Security {
    public class Pbkdf2HashedPassword : HashedPassword {
        private const int HASHING_ITERATIONS = 1000;

        public Pbkdf2HashedPassword(string password) {
            var salt = GetSalt();
            var hash = GetHash(password, salt, HASHING_ITERATIONS);

            Salt = Convert.ToBase64String(salt);
            Hash = Convert.ToBase64String(hash);
        }

        public Pbkdf2HashedPassword(string password, string salt)
         : this(password, Convert.FromBase64String(salt)) { }

        public Pbkdf2HashedPassword(string password, byte[] salt) {
            Salt = Convert.ToBase64String(salt);
            Hash = Convert.ToBase64String(
                GetHash(password, salt, HASHING_ITERATIONS)
            );
        }

        private byte[] GetSalt() {
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create()) {
                rng.GetBytes(salt);
            }
            return salt;
        }

        private byte[] GetHash(string password, byte[] salt, int iterations) {
            return KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: iterations,
                numBytesRequested: 256 / 8);
        }
    }
}