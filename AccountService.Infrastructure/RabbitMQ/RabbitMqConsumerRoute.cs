namespace AccountService.Infrastructure.RabbitMQ {
    public interface IRabbitMqConsumerRoute<TM> {
        string Queue { get; }
        bool IsExclusive { get; }
        bool IsAutoDelete { get; }
        bool IsDurable { get; }

        string Exchange { get; }
        string RoutingKey { get; }
        RabbitMqExchangeType ExchangeType { get; }
    }

    public class RabbitMqConsumerRoute<TM> : IRabbitMqConsumerRoute<TM> {
        public string Queue { get; set; }
        public bool IsExclusive { get; set; }
        public bool IsAutoDelete { get; set; }
        public bool IsDurable { get; set; }

        public string Exchange { get; set; }
        public string RoutingKey { get; set; } = "";
        public RabbitMqExchangeType ExchangeType { get; set; } = RabbitMqExchangeType.Direct;
    }
}