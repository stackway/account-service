using RabbitMQ.Client;

namespace AccountService.Infrastructure.RabbitMQ {
    public interface IRabbitMqSenderRoute<TM> {
        string Exchange { get; }
        string RoutingKey { get; }
        RabbitMqExchangeType ExchangeType { get; }
        string ContentType { get; }
    }

    public class RabbitMqSenderRoute<TM> : IRabbitMqSenderRoute<TM> {
        public string Exchange { get; set; }
        public string RoutingKey { get; set; } = "";
        public RabbitMqExchangeType ExchangeType { get; set; } = RabbitMqExchangeType.Direct;
        public string ContentType { get; set; } = "application/json";
    }
}