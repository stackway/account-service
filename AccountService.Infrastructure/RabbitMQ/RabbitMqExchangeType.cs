using RabbitMQ.Client;

namespace AccountService.Infrastructure.RabbitMQ {
    public enum RabbitMqExchangeType {
        Direct,
        Fanout
    }

    public class RabbitMqExchangeTypeResolver {
        public static string Resolve(RabbitMqExchangeType type) {
            switch (type) {
                case RabbitMqExchangeType.Direct:
                    return ExchangeType.Direct;
                case RabbitMqExchangeType.Fanout:
                    return ExchangeType.Fanout;
                default:
                    return ExchangeType.Direct;
            }
        }
    }
}